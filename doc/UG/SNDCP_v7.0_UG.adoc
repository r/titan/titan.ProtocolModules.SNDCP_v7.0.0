---
Author: János Kövesdi
Version: 198 17-CNL 113 576, Rev. A
Date: 2013-09-05

---
= SNDCP (V7.0.0)  Protocol Modules for TTCN-3 Toolset with TITAN, User Guide
:author: János Kövesdi
:revnumber: 198 17-CNL 113 576, Rev. A
:revdate: 2013-09-05
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the SNDCP protocol module. The SNDCP protocol module is developed for the TTCN-3 Toolset with TITAN. This document should be read together with Function Specification <<_3, [3]>>.

=== Presumed Knowledge

To use this protocol module the knowledge of the TTCN-3 language <<_1, [1]>> and TITAN Test Executor <<_2, [2]>> is essential.

== System Requirements

Protocol modules are a set of TTCN-3 source code files that can be used as part of TTCN-3 test suites only. Hence, protocol modules alone do not put specific requirements on the system used. However, in order to compile and execute a TTCN-3 test suite using the set of protocol modules the following system requirements must be satisfied:

* TITAN TTCN-3 Test Executor R7A (1.7.pl0) or higher installed. For installation guide see <<_5, [5]>>.

NOTE: This version of the protocol module is not compatible with TITAN releases earlier than R7A.

= Protocol Modules

== Overview

Protocol modules implement the messages structure of the related protocol in a formalized way, using the standard specification language TTCN-3. This allows defining of test data (templates) in the TTCN-3 language <<_1, [1]>> and correctly encoding/decoding messages when executing test suites using the Titan TTCN-3 test environment <<_2, [2]>>.

Protocol modules are using Titan’s RAW encoding attributes <<_4, [4]>> and hence is usable with the Titan test toolset only.

== Installation

The set of protocol modules can be used in developing TTCN-3 test suites using any text editor. However to make the work more efficient a TTCN-3-enabled text editor is recommended (e.g. `nedit`, `xemacs`). Since the SNDCP protocol is used as a part of a TTCN-3 test suite, this requires TTCN-3 Test Executor be installed before the module can be compiled and executed together with other parts of the test suite. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_5, [5]>>.

== Configuration

None.

= Terminology

No specific terminology is used.

= Abbreviations

TTCN-3:: Testing and Test Control Notation version 3

= References

[[_1]]
[1] ETSI ES 201 873-1 v.3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] Protocol Modules for TTCN-3 Toolset with TITAN, Function Specification

[[_4]]
[4] Programmer’s Technical Reference for the TITAN TTCN-3 Test Executor

[[_5]]
[5] Installation Guide for the TITAN TTCN-3 Test Executor

